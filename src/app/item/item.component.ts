import { Component, OnInit } from '@angular/core';
import { ItemsService } from '../items/ItemsService';
import { Router, ActivatedRoute } from '@angular/router';
import { Product } from 'src/models/Product';

import { faPencilAlt, faTrash, faShoppingCart } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  item: Product;

  faPencilAlt = faPencilAlt;
  faTrash = faTrash;
  faShoppingCart = faShoppingCart;

  constructor(private itemService: ItemsService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const id = params.get('id');
      this.itemService.getById(id).subscribe(res => this.item = res);
    });

  }


  deleteItemById(id: number){
    this.itemService.deleteById(this.item.id).subscribe(
      res => {
      //
      console.log('Deleted')
      this.router.navigateByUrl('/items');
      },
      err => {
        console.log(err)
      }
      );
  }
}
