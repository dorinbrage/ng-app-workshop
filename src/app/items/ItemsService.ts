import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Product } from "src/models/Product";

@Injectable({ providedIn: 'root' })
export class ItemsService {
  uri = 'http://localhost:8080/items';

  constructor(private http: HttpClient) {}

  addProduct( product: Product ) {
    return this.http.post<Product>(this.uri, product);
  }

  getById(id: string) {
    return this.http.get<Product>(this.uri + `/${id}`);
  }

  getProducts() {
    return this.http.get<Product[]>(this.uri);
  }

  deleteById(id: number){
    return this.http.delete(this.uri + `/${id}`);
  }
}
