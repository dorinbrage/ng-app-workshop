import { Component, OnInit } from '@angular/core';
import {ItemsService} from './ItemsService';
import { Product } from 'src/models/Product';
import { faTrash, faShoppingCart, faEye } from '@fortawesome/free-solid-svg-icons';
import { CartService } from 'src/services/CartService';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {

  items = Array<Product>();

  faTrash = faTrash;
  faShoppingCart = faShoppingCart;
  faEye = faEye;

  constructor(private itemService: ItemsService, private cartService: CartService) { }

  ngOnInit() {
    this.itemService.getProducts().subscribe(res => this.items = res);
  }

  addItemToCart(product: Product){
    // console.log('addItemToCart')
    this.cartService.addItemToProduct(52, product).subscribe(res=>{console.log(res)});
  }
}
