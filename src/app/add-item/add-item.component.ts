import { Component, OnInit } from '@angular/core';
import {Product} from '../../models/Product';
import { ItemsService } from '../items/ItemsService';



@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {

  currentProduct: Product;

  constructor(private itemService: ItemsService) { }

  addItem() {
    this.itemService.addProduct(this.currentProduct).subscribe(
      product => {
        console.log('Product added');
        this.currentProduct = new Product();
      },
      err => { console.log(err); } );
  }

  ngOnInit() {
    this.currentProduct = new Product();
  }

}
