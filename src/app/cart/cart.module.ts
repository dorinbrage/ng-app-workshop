import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart.component';
import { CartRoutingModule } from './cart-routing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {TableModule} from 'primeng/table';



@NgModule({
  declarations: [CartComponent],
  imports: [
    CommonModule,
    CartRoutingModule,
    FontAwesomeModule,
    TableModule
  ]
})
export class CartModule {}
