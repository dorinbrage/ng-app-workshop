import { Component, OnInit } from '@angular/core';

import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { CartService } from 'src/services/CartService';
import { Cart } from 'src/models/Cart';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  cart: Cart;
  cartId = 52;

  faShoppingCart = faShoppingCart;

  constructor(private cartService: CartService) {}

  ngOnInit() {
    this.cartService.getById(this.cartId).subscribe(res => {
      this.cart = res;
    });
  }
}
