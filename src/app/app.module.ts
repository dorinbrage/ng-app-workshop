import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HomeComponent } from './home/home.component';
import { ItemComponent } from './item/item.component';
import { ItemsComponent } from './items/items.component';

import {ItemsService} from './items/ItemsService';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { environment } from '../environments/environment';

import {
  TkitPortalModule,
  MockAuthModule,
  AuthModule,
  APP_CONFIG,
  AUTH_SERVICE
} from 'portal-lib';

// MockAuthModule = mock security

const authModule = MockAuthModule;

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    HomeComponent,
    ItemComponent,
    ItemsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    FontAwesomeModule,
    TkitPortalModule,
    authModule
  ],
  providers: [ItemsService,
    { provide: APP_CONFIG, useValue: environment }],
  bootstrap: [AppComponent]
})
export class AppModule { }
