import { Product } from './Product';

export class CartItem{
  id: number;
  item: Product;
  quantity: number;
  totalPrice: number;
}
