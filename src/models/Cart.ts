import { Product } from './Product';
import { CartItem } from './CartItem';

export class Cart{
  id: number;
  created: boolean;
  items: Set<CartItem>;
}
