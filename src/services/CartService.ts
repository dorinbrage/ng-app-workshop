import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cart } from 'src/models/Cart';
import { Product } from 'src/models/Product';

@Injectable({ providedIn: 'root' })
export class CartService {
  uri = 'http://localhost:8080/cart';

  constructor(private http: HttpClient) {}

  addItemToProduct( cartId: number,  product: Product ) {
    return this.http.post<Product>(this.uri + `/${cartId}/addItem/${product.id}`, null);
  }

  getById(id: number) {
    return this.http.get<Cart>(this.uri + `/${id}`);
  }

  deleteById(id: number){
    return this.http.delete(this.uri + `/${id}`);
  }
}
